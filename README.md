# Test task

## Requirements
* PHP 7.3
* Mysql 5.7.29
* Laravel 7.0 framework

## Installation

##### Clone project
> git clone https://i_bingo_i@bitbucket.org/i_bingo_i/laravel-task.git

##### Copy .env file from .env.dist

> `cp .env.example .env`

##### Deploy environment
> `docker-compose build`

> `docker-compose up -d`

##### Installing composer dependencies
> `docker-compose exec php composer install`

##### Run migrations
> `docker-compose exec php artisan migrate --seed`
