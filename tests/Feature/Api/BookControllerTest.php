<?php

declare(strict_types=1);

namespace Tests\Feature\Api;

use App\Author;
use App\Book;
use App\Category;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Str;
use Tests\TestCase;

class BookControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testIndexSuccess()
    {
        $url = route('books.index');

        $result = $this->get($url);

        $result->assertSuccessful();
    }

    public function testShowSuccess()
    {
        $book = factory(Book::class)->create();

        $url = route('books.show', ['book' => $book->id]);

        $result = $this->get($url);

        $result->assertSuccessful();
    }

    public function testStoreSuccess()
    {
        $author = factory(Author::class)->create();
        $category = factory(Category::class)->create();

        $payload = [
            'title' => Str::random(),
            'price' => rand(1, 100),
            'authors' => [$author->id],
            'categories' => [$category->id]
        ];

        $url = route('books.store');

        $result = $this->post($url, $payload);

        $result->assertSuccessful();
        $this->assertDatabaseHas(Book::TABLE_NAME, [
            'title' => $payload['title'],
        ]);
    }
}
