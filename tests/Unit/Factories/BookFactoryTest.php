<?php

declare(strict_types=1);

namespace Tests\Unit\Factories;

use App\Book;
use App\DataTransferObjects\BookCreateDTOInterface;
use App\Factories\BookFactory;
use Illuminate\Support\Str;
use PHPUnit\Framework\TestCase;

class BookFactoryTest extends TestCase
{
    public function testCreateSuccess()
    {
        $bookFactory = new BookFactory();
        $bookCreateDTO = $this->createMock(BookCreateDTOInterface::class);

        $bookCreateDTO->expects($this->once())->method('getTitle')->willReturn(Str::random());
        $bookCreateDTO->expects($this->once())->method('getPrice')->willReturn(rand(1, 100));

        $result = $bookFactory->create($bookCreateDTO);

        $this->assertInstanceOf(Book::class, $result);
    }
}
