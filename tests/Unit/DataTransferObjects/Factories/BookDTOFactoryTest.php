<?php

declare(strict_types=1);

namespace Tests\Unit\DataTransferObjects\Factories;

use App\DataTransferObjects\BookCreateDTO;
use App\DataTransferObjects\BookIndexDTO;
use App\DataTransferObjects\Factories\BookDTOFactory;
use Illuminate\Support\Str;
use PHPUnit\Framework\TestCase;

class BookDTOFactoryTest extends TestCase
{
    public function testCreateIndexDTOSuccess()
    {
        $bookDTOFactory = new BookDTOFactory();
        $data = [];

        $result = $bookDTOFactory->createIndexDTO($data);

        $this->assertInstanceOf(BookIndexDTO::class, $result);
    }

    public function testCreateBookDTOSuccess()
    {
        $bookDTOFactory = new BookDTOFactory();
        $data = [
            'title' => Str::random(),
            'price' => rand(1, 100),
            'authors' => [rand(1, 100)],
            'categories' => [rand(1, 100)]
        ];

        $result = $bookDTOFactory->createBookDTO($data);

        $this->assertInstanceOf(BookCreateDTO::class, $result);
    }
}
