<?php

declare(strict_types=1);

namespace Tests\Unit\DataTransferObjects;

use App\DataTransferObjects\BookCreateDTO;
use Illuminate\Support\Str;
use PHPUnit\Framework\TestCase;

class BookCreateDTOTest extends TestCase
{
    public function testHandlingSuccess()
    {
        $title = Str::random();
        $price = rand(1, 10);
        $authors = [rand(1, 10)];
        $categories = [rand(1, 10)];

        $bookCreateDTO = new BookCreateDTO($title, $price, $authors, $categories);

        $this->assertEquals($title, $bookCreateDTO->getTitle());
        $this->assertEquals($price, $bookCreateDTO->getPrice());
        $this->assertEquals($authors, $bookCreateDTO->getAuthors());
        $this->assertEquals($categories, $bookCreateDTO->getCategories());
    }
}
