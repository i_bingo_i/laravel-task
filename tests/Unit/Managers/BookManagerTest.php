<?php

declare(strict_types=1);

namespace Tests\Unit\Managers;

use App\DataTransferObjects\BookIndexDTOInterface;
use App\DataTransferObjects\Factories\BookDTOFactoryInterface;
use App\Factories\BookFactoryInterface;
use App\Http\Resources\BookCollectionResource;
use App\Http\Resources\Factories\BookResourceFactoryInterface;
use App\Managers\BookManager;
use App\Repositories\BookRepository;
use App\Repositories\BookRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class BookManagerTest extends TestCase
{
    /**
     * @var BookRepositoryInterface|MockObject
     */
    private $bookRepository;
    /**
     * @var BookResourceFactoryInterface|MockObject
     */
    private $bookResourceFactory;
    /**
     * @var BookDTOFactoryInterface|MockObject
     */
    private $DTOFactory;
    /**
     * @var BookFactoryInterface|MockObject
     */
    private $bookFactory;
    /**
     * @var BookManager
     */
    private $bookManager;

    protected function setUp(): void
    {
        $this->bookRepository = $this->createMock(BookRepository::class);
        $this->bookResourceFactory = $this->createMock(BookResourceFactoryInterface::class);
        $this->DTOFactory = $this->createMock(BookDTOFactoryInterface::class);
        $this->bookFactory = $this->createMock(BookFactoryInterface::class);

        $this->bookManager = new BookManager(
            $this->bookRepository,
            $this->bookResourceFactory,
            $this->DTOFactory,
            $this->bookFactory
        );
    }

    public function testIndexSuccess()
    {
        $bookIndexDTO = $this->createMock(BookIndexDTOInterface::class);
        $collection = $this->createMock(Collection::class);
        $bookCollectionResource = $this->createMock(BookCollectionResource::class);
        $data = [];

        $this->DTOFactory->expects($this->once())->method('createIndexDTO')->willReturn($bookIndexDTO);

        $this->bookRepository->expects($this->once())->method('getBooks')->willReturn($collection);

        $this->bookResourceFactory
            ->expects($this->once())
            ->method('createCollection')
            ->willReturn($bookCollectionResource);

        $result = $this->bookManager->index($data);

        $this->assertInstanceOf(BookCollectionResource::class, $result);
    }
}
