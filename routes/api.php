<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('books', 'Api\BookController')->only(['index', 'show', 'store']);
Route::resource('authors', 'Api\AuthorController')->only(['index', 'show', 'store']);
Route::resource('categories', 'Api\CategoryController')->only(['index', 'show', 'store', 'update']);
