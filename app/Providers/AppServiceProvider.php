<?php

namespace App\Providers;

use App\Author;
use App\Book;
use App\Category;
use App\DataTransferObjects\Factories\AuthorDTOFactory;
use App\DataTransferObjects\Factories\AuthorDTOFactoryInterface;
use App\DataTransferObjects\Factories\BookDTOFactory;
use App\DataTransferObjects\Factories\BookDTOFactoryInterface;
use App\DataTransferObjects\Factories\CategoryDTOFactory;
use App\DataTransferObjects\Factories\CategoryDTOFactoryInterface;
use App\Factories\AuthorFactory;
use App\Factories\AuthorFactoryInterface;
use App\Factories\BookFactory;
use App\Factories\BookFactoryInterface;
use App\Factories\CategoryFactory;
use App\Factories\CategoryFactoryInterface;
use App\Http\Resources\Factories\AuthorResourceFactory;
use App\Http\Resources\Factories\AuthorResourceFactoryInterface;
use App\Http\Resources\Factories\BookResourceFactory;
use App\Http\Resources\Factories\BookResourceFactoryInterface;
use App\Http\Resources\Factories\CategoryResourceFactory;
use App\Http\Resources\Factories\CategoryResourceFactoryInterface;
use App\Managers\AuthorManager;
use App\Managers\AuthorManagerInterface;
use App\Managers\BookManager;
use App\Managers\BookManagerInterface;
use App\Managers\CategoryManager;
use App\Managers\CategoryManagerInterface;
use App\Repositories\AuthorRepository;
use App\Repositories\AuthorRepositoryInterface;
use App\Repositories\BookRepository;
use App\Repositories\BookRepositoryInterface;
use App\Repositories\CategoryRepository;
use App\Repositories\CategoryRepositoryInterface;
use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(BookResourceFactoryInterface::class, function (Application $app) {
            return new BookResourceFactory();
        });

        $this->app->bind(BookRepositoryInterface::class, function (Application $app) {
            return new BookRepository(
                $app->get(Book::class)
            );
        });

        $this->app->bind(BookDTOFactoryInterface::class, function (Application $app) {
            return new BookDTOFactory();
        });

        $this->app->bind(BookFactoryInterface::class, function (Application $app) {
            return new BookFactory();
        });

        $this->app->bind(BookManagerInterface::class, function (Application $app) {
            return new BookManager(
                $app->get(BookRepositoryInterface::class),
                $app->get(BookResourceFactoryInterface::class),
                $app->get(BookDTOFactoryInterface::class),
                $app->get(BookFactoryInterface::class)
            );
        });

        $this->app->bind(AuthorRepositoryInterface::class, function (Application $app) {
            return new AuthorRepository($app->get(Author::class));
        });

        $this->app->bind(AuthorResourceFactoryInterface::class, function (Application $app) {
            return new AuthorResourceFactory();
        });

        $this->app->bind(AuthorDTOFactoryInterface::class, function (Application $app) {
            return new AuthorDTOFactory();
        });

        $this->app->bind(AuthorFactoryInterface::class, function (Application $app) {
            return new AuthorFactory();
        });

        $this->app->bind(AuthorManagerInterface::class, function (Application $app) {
            return new AuthorManager(
                $app->get(AuthorResourceFactoryInterface::class),
                $app->get(AuthorRepositoryInterface::class),
                $app->get(AuthorDTOFactoryInterface::class),
                $app->get(AuthorFactoryInterface::class)
            );
        });

        $this->app->bind(CategoryRepositoryInterface::class, function (Application $app) {
            return new CategoryRepository(
                $app->get(Category::class)
            );
        });

        $this->app->bind(CategoryResourceFactoryInterface::class, function (Application $app) {
            return new CategoryResourceFactory();
        });

        $this->app->bind(CategoryDTOFactoryInterface::class, function (Application $app) {
            return new CategoryDTOFactory();
        });

        $this->app->bind(CategoryFactoryInterface::class, function (Application $app) {
            return new CategoryFactory();
         });

        $this->app->bind(CategoryManagerInterface::class, function (Application $app) {
            return new CategoryManager(
                $app->get(CategoryRepositoryInterface::class),
                $app->get(CategoryResourceFactoryInterface::class),
                $app->get(CategoryDTOFactoryInterface::class),
                $app->get(CategoryFactoryInterface::class)
            );
        });
    }
}
