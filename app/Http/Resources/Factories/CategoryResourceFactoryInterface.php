<?php

declare(strict_types=1);

namespace App\Http\Resources\Factories;

use App\Category;
use App\Http\Resources\CategoryCollectionResource;
use App\Http\Resources\CategoryResource;
use Illuminate\Database\Eloquent\Collection;

interface CategoryResourceFactoryInterface
{
    /**
     * @param Category $category
     * @return CategoryResource
     */
    public function create(Category $category): CategoryResource;

    /**
     * @param Collection $collection
     * @return CategoryCollectionResource
     */
    public function createCollection(Collection $collection): CategoryCollectionResource;
}
