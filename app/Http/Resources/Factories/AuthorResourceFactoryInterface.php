<?php

declare(strict_types=1);

namespace App\Http\Resources\Factories;

use App\Author;
use App\Http\Resources\AuthorCollectionResource;
use App\Http\Resources\AuthorResource;
use Illuminate\Database\Eloquent\Collection;

interface AuthorResourceFactoryInterface
{
    /**
     * @param Author $author
     * @return AuthorResource
     */
    public function create(Author $author): AuthorResource;

    /**
     * @param Collection $collection
     * @return AuthorCollectionResource
     */
    public function createCollection(Collection $collection): AuthorCollectionResource;
}
