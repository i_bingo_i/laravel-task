<?php

declare(strict_types=1);

namespace App\Http\Resources\Factories;

use App\Book;
use App\Http\Resources\BookCollectionResource;
use App\Http\Resources\BookResource;
use Illuminate\Database\Eloquent\Collection;

interface BookResourceFactoryInterface
{
    /**
     * @param Collection $collection
     * @return BookCollectionResource
     */
    public function createCollection(Collection $collection): BookCollectionResource;

    /**
     * @param Book $books
     * @return BookResource
     */
    public function create(Book $books): BookResource;
}
