<?php

declare(strict_types=1);

namespace App\Http\Resources\Factories;

use App\Book;
use App\Http\Resources\BookCollectionResource;
use App\Http\Resources\BookResource;
use Illuminate\Database\Eloquent\Collection;

class BookResourceFactory implements BookResourceFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createCollection(Collection $collection): BookCollectionResource
    {
        return new BookCollectionResource($collection);
    }

    /**
     * @inheritDoc
     */
    public function create(Book $books): BookResource
    {
        return new BookResource($books);
    }
}
