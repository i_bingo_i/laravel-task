<?php

declare(strict_types=1);

namespace App\Http\Resources\Factories;

use App\Author;
use App\Http\Resources\AuthorCollectionResource;
use App\Http\Resources\AuthorResource;
use Illuminate\Database\Eloquent\Collection;

class AuthorResourceFactory implements AuthorResourceFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(Author $author): AuthorResource
    {
        return new AuthorResource($author);
    }

    /**
     * @inheritDoc
     */
    public function createCollection(Collection $collection): AuthorCollectionResource
    {
        return new AuthorCollectionResource($collection);
    }
}
