<?php

declare(strict_types=1);

namespace App\Http\Resources\Factories;

use App\Category;
use App\Http\Resources\CategoryCollectionResource;
use App\Http\Resources\CategoryResource;
use Illuminate\Database\Eloquent\Collection;

class CategoryResourceFactory implements CategoryResourceFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(Category $category): CategoryResource
    {
        return new CategoryResource($category);
    }

    /**
     * @inheritDoc
     */
    public function createCollection(Collection $collection): CategoryCollectionResource
    {
        return new CategoryCollectionResource($collection);
    }
}
