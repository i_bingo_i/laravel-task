<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Rules\AllowOrderRule;
use Illuminate\Foundation\Http\FormRequest;

class BookIndexRequest extends FormRequest
{
    const ALLOW_FIELD_NAMES = ['title', 'author', 'category'];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'needle' => ['string'],
            'order' => [
                'array',
                new AllowOrderRule(self::ALLOW_FIELD_NAMES)
            ]
        ];
    }
}
