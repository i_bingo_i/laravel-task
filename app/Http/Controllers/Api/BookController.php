<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Book;
use App\Http\Controllers\Controller;
use App\Http\Requests\BookIndexRequest;
use App\Http\Requests\BookStoreRequest;
use App\Http\Resources\BookCollectionResource;
use App\Http\Resources\BookResource;
use App\Managers\BookManagerInterface;

class BookController extends Controller
{
    /**
     * @var BookManagerInterface
     */
    public $bookManager;

    /**
     * BookController constructor.
     * @param BookManagerInterface $bookManager
     */
    public function __construct(BookManagerInterface $bookManager)
    {
        $this->bookManager = $bookManager;
    }

    /**
     * @param BookIndexRequest $request
     * @return BookCollectionResource
     */
    public function index(BookIndexRequest $request): BookCollectionResource
    {
        return $this->bookManager->index($request->all());
    }

    /**
     * @param BookStoreRequest $request
     * @return BookResource
     */
    public function store(BookStoreRequest $request): BookResource
    {
        return $this->bookManager->store($request->all());
    }

    /**
     * @param Book $book
     * @return BookResource
     */
    public function show(Book $book): BookResource
    {
        return $this->bookManager->show($book);
    }
}
