<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Author;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuthorStoreRequest;
use App\Http\Resources\AuthorCollectionResource;
use App\Http\Resources\AuthorResource;
use App\Managers\AuthorManagerInterface;

class AuthorController extends Controller
{
    /**
     * @var AuthorManagerInterface
     */
    private $authorManger;

    /**
     * AuthorController constructor.
     * @param AuthorManagerInterface $authorManger
     */
    public function __construct(AuthorManagerInterface $authorManger)
    {
        $this->authorManger = $authorManger;
    }

    /**
     * @return AuthorCollectionResource
     */
    public function index(): AuthorCollectionResource
    {
        return $this->authorManger->index();
    }

    /**
     * @param AuthorStoreRequest $request
     * @return AuthorResource
     */
    public function store(AuthorStoreRequest $request): AuthorResource
    {
        return $this->authorManger->store($request->all());
    }

    /**
     * @param Author $author
     * @return AuthorResource
     */
    public function show(Author $author): AuthorResource
    {
        return $this->authorManger->show($author);
    }
}
