<?php

declare(strict_types=1);

namespace App\Http\Controllers\Api;

use App\Category;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryCreateRequest;
use App\Http\Requests\CategoryUpdateRequest;
use App\Http\Resources\CategoryCollectionResource;
use App\Http\Resources\CategoryResource;
use App\Managers\CategoryManagerInterface;

class CategoryController extends Controller
{
    /**
     * @var CategoryManagerInterface
     */
    private $categoryManager;

    /**
     * CategoryController constructor.
     * @param CategoryManagerInterface $categoryManager
     */
    public function __construct(CategoryManagerInterface $categoryManager)
    {
        $this->categoryManager = $categoryManager;
    }

    /**
     * @return CategoryCollectionResource
     */
    public function index(): CategoryCollectionResource
    {
        return $this->categoryManager->index();
    }

    /**
     * @param CategoryCreateRequest $request
     * @return CategoryResource
     */
    public function store(CategoryCreateRequest $request): CategoryResource
    {
        return $this->categoryManager->store($request->all());
    }

    /**
     * @param Category $category
     * @return CategoryResource
     */
    public function show(Category $category): CategoryResource
    {
        return $this->categoryManager->show($category);
    }

    /**
     * @param Category $category
     * @param CategoryUpdateRequest $request
     * @return CategoryResource
     */
    public function update(Category $category, CategoryUpdateRequest $request): CategoryResource
    {
        return $this->categoryManager->update($category, $request->all());
    }
}
