<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @property int book_id
 * @property int author_id
 * @property int category_id
 */
class Proposal extends Model
{
    const TABLE_NAME = 'proposals';
    const MORPH_TO_NAME = 'proposalable';

    /**
     * @inheritDoc
     */
    public function getTable()
    {
        return self::TABLE_NAME;
    }
}
