<?php

declare(strict_types=1);

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class AllowOrderRule implements Rule
{
    const ALLOW_DIRECTIONS = ['asc', 'desc'];

    /**
     * @var array
     */
    private $allowFieldNames;

    /**
     * @var string
     */
    private $messages;

    /**
     * Create a new rule instance.
     *
     * @param array $allowFieldNames
     */
    public function __construct(array $allowFieldNames)
    {
        $this->allowFieldNames = $allowFieldNames;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        if (true === is_array($value)) {
            foreach ($value as $key => $item) {
                if (false === in_array($key, $this->allowFieldNames)) {
                    return false;
                }

                if (false === in_array($item, self::ALLOW_DIRECTIONS)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message(): string
    {
        return "The field :attribute has wrong field name or direction";
    }
}
