<?php

declare(strict_types=1);

namespace App\Managers;

use App\Book;
use App\Http\Resources\BookCollectionResource;
use App\Http\Resources\BookResource;

interface BookManagerInterface
{
    /**
     * @param array $data
     * @return BookCollectionResource
     */
    public function index(array $data): BookCollectionResource;

    /**
     * @param array $data
     * @return BookResource
     */
    public function store(array $data): BookResource;

    /**
     * @param Book $book
     * @return BookResource
     */
    public function show(Book $book): BookResource;
}
