<?php

declare(strict_types=1);

namespace App\Managers;

use App\Author;
use App\DataTransferObjects\Factories\AuthorDTOFactoryInterface;
use App\Factories\AuthorFactoryInterface;
use App\Http\Resources\AuthorCollectionResource;
use App\Http\Resources\AuthorResource;
use App\Http\Resources\Factories\AuthorResourceFactoryInterface;
use App\Repositories\AuthorRepository;
use App\Repositories\AuthorRepositoryInterface;

/**
 * Class AuthorManager
 * @package App\Managers
 *
 * @property AuthorRepository $authorRepository
 */
class AuthorManager implements AuthorManagerInterface
{
    /**
     * @var AuthorResourceFactoryInterface
     */
    private $authorResourceFactory;

    /**
     * @var AuthorRepositoryInterface
     */
    private $authorRepository;

    /**
     * @var AuthorDTOFactoryInterface
     */
    private $DTOFactory;

    /**
     * @var AuthorFactoryInterface
     */
    private $authorFactory;

    /**
     * AuthorManager constructor.
     * @param AuthorResourceFactoryInterface $authorResourceFactory
     * @param AuthorRepositoryInterface $authorRepository
     * @param AuthorDTOFactoryInterface $DTOFactory
     * @param AuthorFactoryInterface $authorFactory
     */
    public function __construct(
        AuthorResourceFactoryInterface $authorResourceFactory,
        AuthorRepositoryInterface $authorRepository,
        AuthorDTOFactoryInterface $DTOFactory,
        AuthorFactoryInterface $authorFactory
    )
    {
        $this->authorResourceFactory = $authorResourceFactory;
        $this->authorRepository = $authorRepository;
        $this->DTOFactory = $DTOFactory;
        $this->authorFactory = $authorFactory;
    }

    /**
     * @inheritDoc
     */
    public function index(): AuthorCollectionResource
    {
        $authors = $this->authorRepository->getModel()::all();

        return $this->authorResourceFactory->createCollection($authors);
    }

    /**
     * @inheritDoc
     */
    public function store(array $data): AuthorResource
    {
        $authorCreateDTO = $this->DTOFactory->createAuthorDTO($data);

        $author = $this->authorFactory->create($authorCreateDTO);
        $author->save();

        if (null !== $authorCreateDTO->getBooks()) {
            $author->books()->attach($authorCreateDTO->getBooks());
        }

        return $this->authorResourceFactory->create($author);
    }

    /**
     * @inheritDoc
     */
    public function show(Author $author): AuthorResource
    {
        return $this->authorResourceFactory->create($author);
    }
}
