<?php

declare(strict_types=1);

namespace App\Managers;

use App\Book;
use App\DataTransferObjects\Factories\BookDTOFactoryInterface;
use App\Factories\BookFactoryInterface;
use App\Http\Resources\BookCollectionResource;
use App\Http\Resources\BookResource;
use App\Http\Resources\Factories\BookResourceFactoryInterface;
use App\Repositories\BookRepository;
use App\Repositories\BookRepositoryInterface;

/**
 * Class BookManager
 * @package App\Managers
 *
 * @property BookRepository $bookRepository
 */
class BookManager implements BookManagerInterface
{
    /**
     * @var BookRepositoryInterface
     */
    private $bookRepository;

    /**
     * @var BookResourceFactoryInterface
     */
    private $bookResourceFactory;

    /**
     * @var BookDTOFactoryInterface
     */
    private $DTOFactory;

    /**
     * @var BookFactoryInterface
     */
    private $bookFactory;

    /**
     * BookManager constructor.
     * @param BookRepositoryInterface $bookRepository
     * @param BookResourceFactoryInterface $bookResourceFactory
     * @param BookDTOFactoryInterface $DTOFactory
     * @param BookFactoryInterface $bookFactory
     */
    public function __construct(
        BookRepositoryInterface $bookRepository,
        BookResourceFactoryInterface $bookResourceFactory,
        BookDTOFactoryInterface $DTOFactory,
        BookFactoryInterface $bookFactory
    )
    {
        $this->bookRepository = $bookRepository;
        $this->bookResourceFactory = $bookResourceFactory;
        $this->DTOFactory = $DTOFactory;
        $this->bookFactory = $bookFactory;
    }

    /**
     * @inheritDoc
     */
    public function index(array $data): BookCollectionResource
    {
        $bookIndexDTO = $this->DTOFactory->createIndexDTO($data);

        $books = $this->bookRepository->getBooks($bookIndexDTO);

        return $this->bookResourceFactory->createCollection($books);
    }

    /**
     * @inheritDoc
     */
    public function store(array $data): BookResource
    {
        $bookCreateDTO = $this->DTOFactory->createBookDTO($data);

        $book = $this->bookFactory->create($bookCreateDTO);
        $book->save();

        $book->authors()->attach($bookCreateDTO->getAuthors());
        $book->categories()->attach($bookCreateDTO->getCategories());

        return $this->bookResourceFactory->create($book);
    }

    /**
     * @inheritDoc
     */
    public function show(Book $book): BookResource
    {
        return $this->bookResourceFactory->create($book);
    }
}
