<?php

declare(strict_types=1);

namespace App\Managers;

use App\Category;
use App\DataTransferObjects\Factories\CategoryDTOFactoryInterface;
use App\Factories\CategoryFactoryInterface;
use App\Http\Resources\CategoryCollectionResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\Factories\CategoryResourceFactoryInterface;
use App\Repositories\CategoryRepository;
use App\Repositories\CategoryRepositoryInterface;

/**
 * Class CategoryManager
 * @package App\Managers
 *
 * @property CategoryRepository $categoryRepository
 */
class CategoryManager implements CategoryManagerInterface
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $categoryRepository;

    /**
     * @var CategoryResourceFactoryInterface
     */
    private $categoryResourceFactory;

    /**
     * @var CategoryDTOFactoryInterface
     */
    private $categoryDTOFactory;

    /**
     * @var CategoryFactoryInterface
     */
    private $categoryFactory;

    /**
     * CategoryManager constructor.
     * @param CategoryRepositoryInterface $categoryRepository
     * @param CategoryResourceFactoryInterface $categoryResourceFactory
     * @param CategoryDTOFactoryInterface $categoryDTOFactory
     * @param CategoryFactoryInterface $categoryFactory
     */
    public function __construct(
        CategoryRepositoryInterface $categoryRepository,
        CategoryResourceFactoryInterface $categoryResourceFactory,
        CategoryDTOFactoryInterface $categoryDTOFactory,
        CategoryFactoryInterface $categoryFactory
    )
    {
        $this->categoryRepository = $categoryRepository;
        $this->categoryResourceFactory = $categoryResourceFactory;
        $this->categoryDTOFactory = $categoryDTOFactory;
        $this->categoryFactory = $categoryFactory;
    }

    /**
     * @inheritDoc
     */
    public function index(): CategoryCollectionResource
    {
        $categories = $this->categoryRepository->getModel()::all();

        return $this->categoryResourceFactory->createCollection($categories);
    }

    /**
     * @inheritDoc
     */
    public function store(array $date): CategoryResource
    {
        $categoryCreateDTO = $this->categoryDTOFactory->createDTO($date);

        $category = $this->categoryFactory->create($categoryCreateDTO);
        $category->save();

        if (null !== $categoryCreateDTO->getBooks()) {
            $category->books()->attach($categoryCreateDTO->getBooks());
        }

        return $this->categoryResourceFactory->create($category);
    }

    /**
     * @inheritDoc
     */
    public function show(Category $category): CategoryResource
    {
        return $this->categoryResourceFactory->create($category);
    }

    /**
     * @inheritDoc
     */
    public function update(Category $category, array $data): CategoryResource
    {
        $categoryUpdateDTO = $this->categoryDTOFactory->createUpdateDTO($data);

        $category->name = $categoryUpdateDTO->getName() ?? $category->name;
        $category->alias = $categoryUpdateDTO->getAlias() ?? $category->alias;
        $category->save();

        if (null !== $categoryUpdateDTO->getBooks()) {
            $category->books()->detach();
            $category->books()->attach($categoryUpdateDTO->getBooks());
        }

        return $this->categoryResourceFactory->create($category);
    }
}
