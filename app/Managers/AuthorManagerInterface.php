<?php

declare(strict_types=1);

namespace App\Managers;

use App\Author;
use App\Http\Resources\AuthorCollectionResource;
use App\Http\Resources\AuthorResource;

interface AuthorManagerInterface
{
    /**
     * @return AuthorCollectionResource
     */
    public function index(): AuthorCollectionResource;

    /**
     * @param array $data
     * @return AuthorResource
     */
    public function store(array $data): AuthorResource;

    /**
     * @param Author $author
     * @return AuthorResource
     */
    public function show(Author $author): AuthorResource;
}
