<?php

declare(strict_types=1);

namespace App\Managers;

use App\Category;
use App\Http\Resources\CategoryCollectionResource;
use App\Http\Resources\CategoryResource;

interface CategoryManagerInterface
{
    /**
     * @return CategoryCollectionResource
     */
    public function index(): CategoryCollectionResource;

    /**
     * @param array $date
     * @return CategoryResource
     */
    public function store(array $date): CategoryResource;

    /**
     * @param Category $category
     * @return CategoryResource
     */
    public function show(Category $category): CategoryResource;

    /**
     * @param Category $category
     * @param array $data
     * @return CategoryResource
     */
    public function update(Category $category, array $data): CategoryResource;
}
