<?php

declare(strict_types=1);

namespace App\Factories;

use App\Book;
use App\DataTransferObjects\BookCreateDTOInterface;

class BookFactory implements BookFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(BookCreateDTOInterface $bookCreateDTO): Book
    {
        $book = new Book();
        $book->title = $bookCreateDTO->getTitle();
        $book->price = $bookCreateDTO->getPrice();

        return $book;
    }
}
