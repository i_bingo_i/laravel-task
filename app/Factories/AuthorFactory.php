<?php

declare(strict_types=1);

namespace App\Factories;

use App\Author;
use App\DataTransferObjects\AuthorCreateDTOInterface;

class AuthorFactory implements AuthorFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(AuthorCreateDTOInterface $authorCreateDTO): Author
    {
        $author = new Author();
        $author->name = $authorCreateDTO->getName();
        $author->email = $authorCreateDTO->getEmail();

        return $author;
    }
}
