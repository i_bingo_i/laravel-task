<?php

declare(strict_types=1);

namespace App\Factories;

use App\Book;
use App\DataTransferObjects\BookCreateDTOInterface;

interface BookFactoryInterface
{
    /**
     * @param BookCreateDTOInterface $bookCreateDTO
     * @return Book
     */
    public function create(BookCreateDTOInterface $bookCreateDTO): Book;
}
