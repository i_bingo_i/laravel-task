<?php

declare(strict_types=1);

namespace App\Factories;

use App\Author;
use App\DataTransferObjects\AuthorCreateDTOInterface;

interface AuthorFactoryInterface
{
    /**
     * @param AuthorCreateDTOInterface $authorCreateDTO
     * @return Author
     */
    public function create(AuthorCreateDTOInterface $authorCreateDTO): Author;
}
