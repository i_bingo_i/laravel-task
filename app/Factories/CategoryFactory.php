<?php

declare(strict_types=1);

namespace App\Factories;

use App\Category;
use App\DataTransferObjects\CategoryCreateDTOInterface;

class CategoryFactory implements CategoryFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function create(CategoryCreateDTOInterface $categoryCreateDTO): Category
    {
        $category = new Category();
        $category->name = $categoryCreateDTO->getName();
        $category->alias = $categoryCreateDTO->getAlias();

        return $category;
    }
}
