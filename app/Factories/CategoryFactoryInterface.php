<?php

declare(strict_types=1);

namespace App\Factories;

use App\Category;
use App\DataTransferObjects\CategoryCreateDTOInterface;

interface CategoryFactoryInterface
{
    /**
     * @param CategoryCreateDTOInterface $categoryCreateDTO
     * @return Category
     */
    public function create(CategoryCreateDTOInterface $categoryCreateDTO): Category;
}
