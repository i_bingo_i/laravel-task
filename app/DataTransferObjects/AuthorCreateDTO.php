<?php

declare(strict_types=1);

namespace App\DataTransferObjects;

class AuthorCreateDTO implements AuthorCreateDTOInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $email;

    /**
     * @var array|null
     */
    private $books;

    /**
     * AuthorCreateDTO constructor.
     * @param string $name
     * @param string $email
     * @param array|null $books
     */
    public function __construct(string $name, string $email, ?array $books)
    {
        $this->name = $name;
        $this->email = $email;
        $this->books = $books;
    }

    /**
     * @inheritDoc
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @inheritDoc
     */
    public function getBooks(): ?array
    {
        return $this->books;
    }
}
