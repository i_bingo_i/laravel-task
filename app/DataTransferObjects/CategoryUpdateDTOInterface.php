<?php

declare(strict_types=1);

namespace App\DataTransferObjects;

interface CategoryUpdateDTOInterface
{
    /**
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * @return string|null
     */
    public function getAlias(): ?string;

    /**
     * @return array|null
     */
    public function getBooks(): ?array;
}
