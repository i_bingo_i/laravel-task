<?php

declare(strict_types=1);

namespace App\DataTransferObjects;

interface CategoryCreateDTOInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getAlias(): string;

    /**
     * @return array|null
     */
    public function getBooks(): ?array;
}
