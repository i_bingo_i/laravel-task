<?php

declare(strict_types=1);

namespace App\DataTransferObjects;

class BookCreateDTO implements BookCreateDTOInterface
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var int
     */
    private $price;

    /**
     * @var array
     */
    private $authors;

    /**
     * @var array
     */
    private $categories;

    /**
     * BookCreateDTO constructor.
     * @param string $title
     * @param int $price
     * @param array $authors
     * @param array $categories
     */
    public function __construct(string $title, int $price, array $authors, array $categories)
    {
        $this->title = $title;
        $this->price = $price;
        $this->authors = $authors;
        $this->categories = $categories;
    }

    /**
     * @inheritDoc
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @inheritDoc
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @inheritDoc
     */
    public function getAuthors(): array
    {
        return $this->authors;
    }

    /**
     * @inheritDoc
     */
    public function getCategories(): array
    {
        return $this->categories;
    }
}
