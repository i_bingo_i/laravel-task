<?php

declare(strict_types=1);

namespace App\DataTransferObjects;

class BookIndexDTO implements BookIndexDTOInterface
{
    /**
     * @var null|string
     */
    private $needle;

    /**
     * @var null|array
     */
    private $order;

    /**
     * BookIndexDTO constructor.
     * @param null|string $needle
     * @param null|array $order
     */
    public function __construct(?string $needle, ?array $order)
    {
        $this->needle = $needle;
        $this->order = $order;
    }

    /**
     * @inheritDoc
     */
    public function getNeedle(): ?string
    {
        return $this->needle;
    }

    /**
     * @inheritDoc
     */
    public function getOrder(): ?array
    {
        return $this->order;
    }
}
