<?php

declare(strict_types=1);

namespace App\DataTransferObjects;

interface BookIndexDTOInterface
{
    /**
     * @return null|string
     */
    public function getNeedle(): ?string;

    /**
     * @return null|array
     */
    public function getOrder(): ?array;
}
