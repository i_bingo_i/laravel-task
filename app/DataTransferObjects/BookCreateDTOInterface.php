<?php

declare(strict_types=1);

namespace App\DataTransferObjects;

interface BookCreateDTOInterface
{
    /**
     * @return string
     */
    public function getTitle(): string;

    /**
     * @return int
     */
    public function getPrice(): int;

    /**
     * @return array
     */
    public function getAuthors(): array;

    /**
     * @return array
     */
    public function getCategories(): array;
}
