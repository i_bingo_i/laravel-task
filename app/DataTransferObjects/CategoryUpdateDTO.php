<?php

declare(strict_types=1);

namespace App\DataTransferObjects;

class CategoryUpdateDTO implements CategoryUpdateDTOInterface
{
    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $alias;

    /**
     * @var array|null
     */
    private $books;

    /**
     * CategoryIndexDTO constructor.
     * @param string $name
     * @param string $alias
     * @param array|null $books
     */
    public function __construct(?string $name, ?string $alias, ?array $books)
    {
        $this->name = $name;
        $this->alias = $alias;
        $this->books = $books;
    }

    /**
     * @inheritDoc
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @inheritDoc
     */
    public function getAlias(): ?string
    {
        return $this->alias;
    }

    /**
     * @inheritDoc
     */
    public function getBooks(): ?array
    {
        return $this->books;
    }
}
