<?php

declare(strict_types=1);

namespace App\DataTransferObjects;

interface AuthorCreateDTOInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getEmail(): string;

    /**
     * @return array|null
     */
    public function getBooks(): ?array;
}
