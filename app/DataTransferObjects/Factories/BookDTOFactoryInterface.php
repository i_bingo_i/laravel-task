<?php

declare(strict_types=1);

namespace App\DataTransferObjects\Factories;

use App\DataTransferObjects\BookCreateDTOInterface;
use App\DataTransferObjects\BookIndexDTOInterface;

interface BookDTOFactoryInterface
{
    /**
     * @param array $data
     * @return BookIndexDTOInterface
     */
    public function createIndexDTO(array $data): BookIndexDTOInterface;

    /**
     * @param array $data
     * @return BookCreateDTOInterface
     */
    public function createBookDTO(array $data): BookCreateDTOInterface;
}
