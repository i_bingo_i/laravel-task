<?php

declare(strict_types=1);

namespace App\DataTransferObjects\Factories;

use App\DataTransferObjects\CategoryCreateDTO;
use App\DataTransferObjects\CategoryCreateDTOInterface;
use App\DataTransferObjects\CategoryUpdateDTO;
use App\DataTransferObjects\CategoryUpdateDTOInterface;
use Illuminate\Support\Str;

class CategoryDTOFactory implements CategoryDTOFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createUpdateDTO(array $data): CategoryUpdateDTOInterface
    {
        return new CategoryUpdateDTO(
            $data['name'] ?? null,
            isset($data['name']) ? Str::slug($data['name'], '-') : null,
            $data['books'] ?? null
        );
    }

    /**
     * @inheritDoc
     */
    public function createDTO(array $data): CategoryCreateDTOInterface
    {
        return new CategoryCreateDTO(
            $data['name'],
            Str::slug($data['name'], '-'),
            $data['books'] ?? null
        );
    }
}
