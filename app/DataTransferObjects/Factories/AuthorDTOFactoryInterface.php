<?php

declare(strict_types=1);

namespace App\DataTransferObjects\Factories;

use App\DataTransferObjects\AuthorCreateDTOInterface;

interface AuthorDTOFactoryInterface
{
    /**
     * @param array $data
     * @return AuthorCreateDTOInterface
     */
    public function createAuthorDTO(array $data): AuthorCreateDTOInterface;
}
