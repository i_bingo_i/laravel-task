<?php

declare(strict_types=1);

namespace App\DataTransferObjects\Factories;

use App\DataTransferObjects\CategoryCreateDTOInterface;
use App\DataTransferObjects\CategoryUpdateDTOInterface;

interface CategoryDTOFactoryInterface
{
    /**
     * @param array $data
     * @return CategoryUpdateDTOInterface
     */
    public function createUpdateDTO(array $data): CategoryUpdateDTOInterface;

    /**
     * @param array $data
     * @return CategoryCreateDTOInterface
     */
    public function createDTO(array $data): CategoryCreateDTOInterface;
}
