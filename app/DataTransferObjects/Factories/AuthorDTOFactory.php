<?php

declare(strict_types=1);

namespace App\DataTransferObjects\Factories;

use App\DataTransferObjects\AuthorCreateDTO;
use App\DataTransferObjects\AuthorCreateDTOInterface;

class AuthorDTOFactory implements AuthorDTOFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createAuthorDTO(array $data): AuthorCreateDTOInterface
    {
        return new AuthorCreateDTO(
            $data['name'],
            $data['email'],
            $data['books'] ?? null
        );
    }
}
