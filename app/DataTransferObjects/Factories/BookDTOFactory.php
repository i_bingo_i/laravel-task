<?php

declare(strict_types=1);

namespace App\DataTransferObjects\Factories;

use App\DataTransferObjects\BookCreateDTO;
use App\DataTransferObjects\BookCreateDTOInterface;
use App\DataTransferObjects\BookIndexDTO;
use App\DataTransferObjects\BookIndexDTOInterface;

class BookDTOFactory implements BookDTOFactoryInterface
{
    /**
     * @inheritDoc
     */
    public function createIndexDTO(array $data): BookIndexDTOInterface
    {
        return new BookIndexDTO(
            $data['needle'] ?? null,
            $data['order'] ?? null
        );
    }

    /**
     * @inheritDoc
     */
    public function createBookDTO(array $data): BookCreateDTOInterface
    {
        return new BookCreateDTO($data['title'], $data['price'], $data['authors'], $data['categories']);
    }
}
