<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property string name
 * @property string alias
 */
class Category extends Model
{
    const TABLE_NAME = 'categories';

    protected $fillable = [
        'name',
        'alias'
    ];
    /**
     * @inheritDoc
     */
    public function getTable()
    {
        return self::TABLE_NAME;
    }

    /**
     * @return BelongsToMany
     */
    public function books(): BelongsToMany
    {
        return $this->belongsToMany(Book::class);
    }
}
