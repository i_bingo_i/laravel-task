<?php

declare(strict_types=1);

namespace App;

interface BookCategoryTableInterface
{
    const TABLE_NAME = 'book_category';
}
