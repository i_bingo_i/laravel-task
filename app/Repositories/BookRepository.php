<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Author;
use App\AuthorBookPivotTableInterface;
use App\Book;
use App\BookCategoryTableInterface;
use App\Category;
use App\DataTransferObjects\BookIndexDTOInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class BookRepository
 * @package App\Repositories
 *
 * @property Book $model
 */
class BookRepository extends BaseRepository implements BookRepositoryInterface
{
    const TABLE_FIELD_NAME_MATCHING = [
        'title' => Book::TABLE_NAME . '.title',
        'author' => 'author_name',
        'category' => 'cat_name'
    ];

    /**
     * @inheritDoc
     */
    public function __construct(Book $model)
    {
        $this->model = $model;
    }

    /**
     * @param BookIndexDTOInterface $bookIndexDTO
     * @return Collection
     */
    public function getBooks(BookIndexDTOInterface $bookIndexDTO): Collection
    {
        if (null !== $bookIndexDTO->getNeedle() || null !== $bookIndexDTO->getOrder()) {
            $query = $this->model->newQuery();
            $query->select(Book::TABLE_NAME . '.*');
            $query->selectRaw('group_concat(`authors`.`name`) as author_name');
            $query->selectRaw('group_concat(`categories`.`name`) as cat_name');
            $query->groupBy(Book::TABLE_NAME . '.id');

            $this->joinCategoryTable($query);
            $this->joinAuthorTable($query);

            if (null !== $bookIndexDTO->getNeedle()) {
                $this->searching($query, $bookIndexDTO->getNeedle());
            }

            if (null !== $bookIndexDTO->getOrder()) {
                $this->ordering($query, $bookIndexDTO->getOrder());
            }

            return $query->get();
        }

        return $this->model::all();
    }

    /**
     * @param Builder $query
     * @param string $needle
     */
    private function searching(Builder $query, string $needle)
    {
       $query->orWhere(Book::TABLE_NAME . '.title', 'like', '%'. $needle . '%');
       $query->orWhere(Category::TABLE_NAME . '.name', 'like', '%'. $needle . '%');
       $query->orWhere(Author::TABLE_NAME . '.name', 'like', '%'. $needle . '%');
    }

    /**
     * @param Builder $query
     * @param array $order
     */
    private function ordering(Builder $query, array $order)
    {
        foreach ($order as $fieldName => $direction) {
            $query->orderBy(self::TABLE_FIELD_NAME_MATCHING[$fieldName], $direction);
        }
    }

    /**
     * @param Builder $query
     */
    private function joinCategoryTable(Builder $query)
    {
        $query->join(
            BookCategoryTableInterface::TABLE_NAME,
            Book::TABLE_NAME . '.id',
            BookCategoryTableInterface::TABLE_NAME . '.book_id'
        );

        $query->join(
            Category::TABLE_NAME,
            BookCategoryTableInterface::TABLE_NAME . '.category_id',
            Category::TABLE_NAME . '.id'
        );
    }

    /**
     * @param Builder $query
     */
    private function joinAuthorTable(Builder $query)
    {
        $query->join(
            AuthorBookPivotTableInterface::TABLE_NAME,
            Book::TABLE_NAME . '.id',
            BookCategoryTableInterface::TABLE_NAME . '.book_id'
        );

        $query->join(
            Author::TABLE_NAME,
            AuthorBookPivotTableInterface::TABLE_NAME . '.author_id',
            Author::TABLE_NAME . '.id'
        );
    }
}
