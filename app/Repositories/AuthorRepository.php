<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Author;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AuthorRepository
 * @package App\Repositories
 *
 * @property Author $model
 */
class AuthorRepository extends BaseRepository implements AuthorRepositoryInterface
{
    /**
     * @inheritDoc
     */
    public function __construct(Author $model)
    {
        $this->model = $model;
    }
}
