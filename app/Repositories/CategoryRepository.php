<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Category;

/**
 * Class CategoryRepository
 * @package App\Repositories
 *
 * @property Category $model
 */
class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{
    /**
     * CategoryRepository constructor.
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->model = $category;
    }
}
