<?php

declare(strict_types=1);

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * Class Author
 * @package App
 * @property string $name
 * @property string $email
 */
class Author extends Model
{
    const TABLE_NAME = 'authors';

    /**
     * @inheritDoc
     */
    public function getTable()
    {
        return self::TABLE_NAME;
    }

    /**
     * @return BelongsToMany
     */
    public function books(): BelongsToMany
    {
        return $this->belongsToMany(Book::class);
    }
}
