<?php

declare(strict_types=1);

namespace App;

interface AuthorBookPivotTableInterface
{
    const TABLE_NAME = 'author_book';
}
