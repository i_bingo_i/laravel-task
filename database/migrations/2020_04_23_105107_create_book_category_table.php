<?php

use App\Book as BookModel;
use App\BookCategoryTableInterface;
use App\Category;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(BookCategoryTableInterface::TABLE_NAME, function (Blueprint $table) {
            $table->bigInteger('book_id')->unsigned();
            $table->bigInteger('category_id')->unsigned();

            $table->foreign(['book_id'])->references('id')->on(BookModel::TABLE_NAME)->cascadeOnDelete();
            $table->foreign(['category_id'])->references('id')->on(Category::TABLE_NAME)->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(BookCategoryTableInterface::TABLE_NAME, function (Blueprint $table) {
            $table->dropForeign(['book_id']);
            $table->dropForeign(['category_id']);
        });
        Schema::dropIfExists(BookCategoryTableInterface::TABLE_NAME);
    }
}
