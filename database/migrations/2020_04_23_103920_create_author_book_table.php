<?php

use App\Author as AuthorModel;
use App\AuthorBookPivotTableInterface;
use App\Book as BookModel;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAuthorBookTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(AuthorBookPivotTableInterface::TABLE_NAME, function (Blueprint $table) {
            $table->bigInteger('book_id')->unsigned();
            $table->bigInteger('author_id')->unsigned();

            $table->foreign(['book_id'])->references('id')->on(BookModel::TABLE_NAME)->cascadeOnDelete();
            $table->foreign(['author_id'])->references('id')->on(AuthorModel::TABLE_NAME)->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(AuthorBookPivotTableInterface::TABLE_NAME, function (Blueprint $table) {
            $table->dropForeign(['book_id']);
            $table->dropForeign(['author_id']);
        });
        Schema::dropIfExists(AuthorBookPivotTableInterface::TABLE_NAME);
    }
}
