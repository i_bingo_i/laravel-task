<?php

declare(strict_types=1);

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Model;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'title' => $faker->title,
        'price' => $faker->randomNumber()
    ];
});
