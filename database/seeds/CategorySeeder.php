<?php

declare(strict_types=1);

use App\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $scienceFictionCategory = new Category();
        $scienceFictionCategory->name = 'Science Fiction';
        $scienceFictionCategory->alias = 'science-fiction';
        $scienceFictionCategory->save();

        $dramaCategory = new Category();
        $dramaCategory->name = 'Drama';
        $dramaCategory->alias = 'drama';
        $dramaCategory->save();

        $adventuresCategory = new Category();
        $adventuresCategory->name = 'Adventures';
        $adventuresCategory->alias = 'adventures';
        $adventuresCategory->save();
    }
}
