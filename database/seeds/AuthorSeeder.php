<?php

declare(strict_types=1);

use App\Author as AuthorModel;
use App\Book as BookModel;
use Illuminate\Database\Seeder;

class AuthorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $books = BookModel::all();
        factory(AuthorModel::class, 5)->create()->each(function (AuthorModel $author) use ($books) {
            $author->books()->attach($books->random(rand(1, 3))->map(function (BookModel $book) {
                return $book->getKey();
            }));
        });
    }
}
