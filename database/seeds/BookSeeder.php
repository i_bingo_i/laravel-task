<?php

declare(strict_types=1);

use App\Book as BookModel;
use App\Category;
use Illuminate\Database\Seeder;

class BookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = Category::all();
        factory(BookModel::class, 5)->create()->each(function (BookModel $book) use ($categories) {
            $book->categories()->attach($categories->random(rand(1, 3))->map(function (Category $category) {
                return $category->getKey();
            }));
        });
    }
}
